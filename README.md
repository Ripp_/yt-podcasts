# yt-podcasts
Turn youtube videos into audio only podcasts, for use with a podcast app

## Setup
* You will need [ffmpeg](https://www.ffmpeg.org/)
* You will need a [Youtube Api V3](https://developers.google.com/youtube/v3) api key to use the tool, follow the [getting started](https://developers.google.com/youtube/v3/getting-started) from youtube. You only need an Api Key, not an OAuth client ID.
* Run `npm install` to install dependencies
* Create a `config.json` file containing the following vales:
    * `apiKey`: Your Youtube Api Key
    * `feed`: default values to use for feed metadata

### Setup podcasts
* Create a folder inside `podcasts` with the name you wish to use to refrence the podcast
* Inside this folder create a `config.json` file containing:
    * `feed`: the values to use for the feed metadata
    * `search`: the search information to locate the videos
* Optionally add an `image` file to the folder containing the image for the podcast

## Usage
* Start the app by running `node .` from inside the directory
* Point your podcast app at `<your url>/podcasts/<name of podcast folder>.rss`
* Episodes download automatically when someone visits the rss feed and a new one is found
* (App currentlly starts on port localhost:3000)

## Feed metadata & Search values:
* Feed options in both config files are passed directly as [npm-podcast's feed options](https://www.npmjs.com/package/podcast#feedoptions), referr there for documentation.
* Search options are passed directly as [youtube search api paramaters](https://developers.google.com/youtube/v3/docs/search/list#parameters), referr there for documentation, except for:
    * `type`: ignored, "video" is used instead
    * `order`: ignored, "date" is used instead
    * `part`: ignored, "snippet" is used instead