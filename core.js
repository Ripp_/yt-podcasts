const fs = require("mz/fs")
const youtubeSearch = require("youtube-api-v3-search");
const podcastFeed = require("podcast");
const ytdl = require("ytdl-core");
const ffmpeg = require("fluent-ffmpeg");
const querystring = require("querystring");

let CONFIG;
(async function () {
    CONFIG = JSON.parse(await fs.readFile("config.json"));
})()

module.exports = {
    startDownload, isAuthValid, createRssFeed
}

/**
 * Beings downloading and converting a video for a podcast.  
 * Will do nothing if the download has already been started/completed.
 * @param {string} podcast name podcast to download to
 * @param {string} videoId id of the youtube video to download
 */
async function startDownload(podcast, videoId) {
    let file;
    try {
        let target = `podcasts/${podcast}/${videoId}.mp3`;
        if (await fs.exists(target)) {
            return;
        }
        console.info(`${podcast}/${videoId} Downloading...`);
        let stream = ytdl(`http://www.youtube.com/watch?v=${videoId}`, { format: "audio" })
        let proc = new ffmpeg({ source: stream, logger: console }).saveToFile(target, function (stdout, stderr) {
            if (stderr) {
                console.error(`${podcast}/${videoId} Error:`, stderr);
            } else {
                console.info(`${podcast}/${videoId} Downloaded!`)
            }
        });
    } catch (e) {
        console.warn(e);
    }
}

/**
 * Checks if the given auth credidentals are present in the whitelist,
 * or if anomonous access is allowed.
 * @param {string} auth Auth code in form `username:password`
 * @returns {boolean} true if there is no whitelist or the credientals are present and correct
 */
function isAuthValid(auth) {
    if (!("whitelist" in CONFIG)) {
        return true;
    }
    if (auth === undefined) {
        return false
    }
    let [user, password] = auth.split(":");
    return CONFIG.whitelist[user] !== undefined && CONFIG.whitelist[user] === password;
}

/**
 * Generates the rss feed for the pdocast.
 * @param {string} podcast The podcast to generate the feed for
 * @param {string} auth Auth code in form `username:password` to check if access is allowed for
 * @param {string} urlPrefix The base path of the URL, should include everything upto, but not including, the podcast's name
 * @returns {string} Rss feed ready for transmission
 */
async function createRssFeed(podcast, auth, urlPrefix) {

    const makeUrl = (...parts) => `${urlPrefix}/${podcast}/${parts.join("")}${authParams}`;

    if (!isAuthValid(auth)) {
        throw { code: 403, error: "Unauthorized" }
    }

    const authParams = auth === undefined ? "" : "?" + querystring.stringify({ auth })

    const configJson = await fs.readFile(`podcasts/${podcast}/config.json`);
    const config = JSON.parse(configJson);

    config.search.part = "snippet";
    config.search.type = "video";
    config.search.order = "date";

    const searchResults = await youtubeSearch(CONFIG.apiKey, config.search);
    const feedResult = new podcastFeed(Object.assign(
        { imageUrl: makeUrl("image") },
        CONFIG.feed,
        config.feed
    ));

    for (const item of searchResults.items) {
        startDownload(podcast, item.id.videoId);
        feedResult.addItem({
            title: item.snippet.title,
            description: item.snippet.description,
            url: `https://youtu.be/${item.id.videoId}`,
            guid: `video:audio:${item.id.videoId}`,
            date: item.snippet.publishedAt,
            enclosure: {
                url: makeUrl(item.id.videoid, ".mp3")
            }
        });
    }
    return feedResult.buildXml();
}