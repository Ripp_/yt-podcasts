const express = require("express")
const fs = require("mz/fs")
const { startDownload, isAuthValid, createRssFeed } = require("./core.js");

const app = express()

app.get("/podcasts/:podcast.rss", async (req, res) => {
    try {
        const { podcast } = req.params;
        const auth = req.query.auth;
        const urlPrefix = `${req.protocol}://${req.get("Host")}/podcasts`;
        const rss = await createRssFeed(podcast, auth, urlPrefix);
        res.end(rss);
    } catch (e) {
        console.error(e);
        if (("code" in e) && ("error" in e)) {
            res.status(e.code).end(e.error);
        } else {
            res.status(404).end(e.toString());
        }
    }
});

app.use("/podcasts/:podcast/:episode.mp3", (req, res) => {
    const { podcast, episode } = req.params;

    if (!isAuthValid(req.query.auth)) {
        res.status(403).end("unathorised");
        return;
    }

    try {
        fs.createReadStream(`podcasts/${podcast}/${episode}.mp3`).pipe(res);
    } catch (e) {
        res.status(404).end();
    }
});

app.use("/podcasts/:podcast/image", (req, res) => {
    if (!isAuthValid(req.query.auth)) {
        res.status(403).end("unathorised");
        return;
    }

    try {
        const { podcast, episode } = req.params;
        fs.createReadStream(`podcasts/${podcast}/image`).pipe(res);
    } catch (e) {
        res.status(404);
    }
});

app.listen(3000, () => console.log("Example app listening on port 3000!"));
